﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using Microsoft.Kinect;
using KinectTracking.src.Track;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Collections;
using System.Threading;
using KinectTracking.src.DataStructures;
using Microsoft.Win32;
using KinectTracking.src.Tools;
using KinectTracking.src.Track.GestureRecognition;
using System.Timers;
using KinectTracking.src.Math;
using KinectTracking.src.Tools.IO;
using System.Windows.Input;
using WpfApplicationHotKey.WinApi;

namespace KinectTracking
{
    public partial class MainWindow : Window
    {
        private KinectSensor kinectSensor = null;
        MultiSourceFrameReader _reader;
        IList<Body> bodies;
        Tracker tracker;
        private Boolean colorCameraOn = false;
        private Boolean bodyCameraOn = true;
        private System.Timers.Timer timer;

        private HotKey _hotkeyMarkFrame;
        private HotKey _hotkey2;

        private const int MinimumDBSize = 30;

        //for testing
        Frames frames;

        public MainWindow()
        {
            InitializeComponent();
            Loaded += (s, e) =>
            {
                _hotkeyMarkFrame = new HotKey(ModifierKeys.None, Keys.B, this);
                _hotkey2 = new HotKey(ModifierKeys.None, Keys.F5, this);
                _hotkeyMarkFrame.HotKeyPressed += (k) => SwitchMarker();
                _hotkey2.HotKeyPressed += (k) => MarkFrame();
            };

            tracker = new Tracker();
            if (tracker.GetRecognitionBuffers().getDB().Count < MinimumDBSize)
            {
                tracker.recognisingGestures = false;
                MessageBox.Show("Database is too small for gesture recognition, gesture recognition is turned off!");
            }

            //event listener for buffer events:
            Buffers buffers = tracker.GetRecognitionBuffers();
            buffers.Recognised += new Buffers.RecognitionHandler(Set_Recognised_Gesture);

            markerCount.Content = tracker.CurrentMarker + "(" + tracker.MarkerCount + ")";

            this.kinectSensor = KinectSensor.GetDefault();

            _reader = kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color |
                                             FrameSourceTypes.Depth |
                                             FrameSourceTypes.Infrared |
                                             FrameSourceTypes.Body);
            _reader.MultiSourceFrameArrived += Reader_MultiSourceFrameArrived;
            
            //kinectSensor.Close();
        }

        private void Reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            var reference = e.FrameReference.AcquireFrame();
            // Depth:..
            // Infrared:..

            // Color:
            if (colorCameraOn)
            {
                using (var frame = reference.ColorFrameReference.AcquireFrame())
                {
                    if (frame != null)
                    {
                        camera.Source = frame.ToBitmap();
                    }
                }
            }

            // Body:
            if (bodyCameraOn)
            {
                using (var frame = reference.BodyFrameReference.AcquireFrame())
                {
                    if (frame != null)
                    {
                        ClearCanvas();
                        bodies = new Body[frame.BodyFrameSource.BodyCount];

                        frame.GetAndRefreshBodyData(bodies);

                        foreach (var body in bodies)
                        {
                            if (body != null)
                            {
                                if (body.IsTracked)
                                {
                                    tracker.saveFrame(body);
                                    canvas.DrawSkeleton(body, topProjectionCanvas, sideProjectionCanvas);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void openCoordLog(object sender, RoutedEventArgs e)
        {
            tracker.openCoordLogFile();
        }

        private void OpenOrientationLog(object sender, RoutedEventArgs e)
        {
            tracker.openOrientationLogFile();
        }

        private void ToggleColorCamera(object sender, RoutedEventArgs e)
        {
            if (kinectSensor != null)
            {
                if (kinectSensor.IsOpen)
                {
                    colorCameraOn = !colorCameraOn;
                    camera.Source = null;
                }
                else
                {
                    MessageBox.Show("Kinect is not recording", "Error");
                }
            }
        }

        private void Close_app(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Open_Log_Folder(object sender, RoutedEventArgs e)
        {
            String logFolderPath = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["logLocationFolder"];
            if(Directory.Exists(logFolderPath))
            {
                Process.Start(logFolderPath);
            }
            else
            {
                MessageBox.Show("Log path does not exist.", "Error");
            }
            
        }

        private void Open_Log(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            bool? userClickedOK = openFileDialog1.ShowDialog();
            if (userClickedOK == true)
            {
                bodyCameraOn = false;
                System.IO.Stream fileStream = null;
                try
                {
                    fileStream = openFileDialog1.OpenFile();
                    frames = LogTools.readCoordinateLog(fileStream);

                    Thread drawBufferThread = new Thread(() => drawBody(frames));
                    drawBufferThread.Start();
                } catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "File read error.");
                } finally
                {
                    if (fileStream != null)
                    {
                        fileStream.Close();
                    }
                }

            }
        }

        //drawing body from buffer
        private void Replay_Button_DrawBuffer(object sender, RoutedEventArgs e)
        {
            bodyCameraOn = false;
            //TODO: õpi kuidas c# threadid töötavad täpselt, praegune poolsuvalt kokku klopsitud
            Thread drawBufferThread = new Thread(() => drawBody());
            drawBufferThread.Start();
        }

        //draw body from buffer
        private void drawBody()
        {
            drawBody((IEnumerator<Frame>)tracker.getBuffer().GetEnumerator());
        }

        private void drawBody(Frames frames)
        {
            drawBody((IEnumerator<Frame>)frames.GetEnumerator());
        }

        private void drawBody(IEnumerator<Frame> enumerator)
        {
            object myLock = new object();
            lock(myLock)
            {
                while (enumerator.MoveNext())
                {
                    try
                    {
                        this.Dispatcher.Invoke((Action)(() =>
                        {
                            ClearCanvas();
                            canvas.DrawBuffer(enumerator.Current, topProjectionCanvas, sideProjectionCanvas);
                            textBlockFrameNr.Text = enumerator.Current.id.ToString();
                        }));
                    }
                    catch (OperationCanceledException e)
                    {
                        /*application stopped during animation, this is not a problem*/
                    }
                    System.Threading.Thread.Sleep(50);//normal speed = 50
                }
            }
            bodyCameraOn = true;
        }

        private void ClearCanvas()
        {
            canvas.Children.Clear();
            topProjectionCanvas.Children.Clear();
            sideProjectionCanvas.Children.Clear();
        }

        private void record_button_Click(object sender, RoutedEventArgs e)
        {
            bodyCameraOn = true;
            if(timer == null || !timer.Enabled)
            {
                timer = new System.Timers.Timer();
                timer.Elapsed += new ElapsedEventHandler(ToggleRecordInidcatorVisibility);
                timer.Interval = 800;
                timer.Enabled = true;

                if (kinectSensor != null)
                {
                    kinectSensor.Open();
                }
            }
        }

        private void stop_record_button_Click(object sender, RoutedEventArgs e)
        {
            timer.Enabled = false;
            this.Dispatcher.Invoke((Action)(() =>
            {
                recordBlinker.Visibility = Visibility.Collapsed;
            }));
            if (kinectSensor != null)
            {
                kinectSensor.Close();
            }
        }

        private void ToggleRecordInidcatorVisibility(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                recordBlinker.Visibility = (recordBlinker.Visibility.Equals(Visibility.Visible)) 
                                                ? Visibility.Collapsed 
                                                : Visibility.Visible;
            }));
        }

        private void MarkFrame()
        {
            if (tracker.GetCurrentFrame() != null)
            {
                string coordinateLog = File.ReadAllText(tracker.GetCoordLogPath());
                string newCoordinateLog = coordinateLog.TrimEnd(Environment.NewLine.ToCharArray());
                File.WriteAllText(tracker.GetCoordLogPath(), newCoordinateLog + "," + GlobalVariables.GestureMark + Environment.NewLine);
            }
        }

        private void calc_bezier_button_Click(object sender, RoutedEventArgs e)
        {
            bodyCameraOn = false;
            this.Topmost = false;
            new BezierCurveLogger().ReadFramesFromFiles();
            this.Topmost = true;
            MessageBox.Show("Ready!");
            bodyCameraOn = true;
        }

        private void calc_distances_button_Click(object sender, RoutedEventArgs e)
        {
            bodyCameraOn = false;
            new DistanceCalculator().ReadFramesFromFiles();
            MessageBox.Show("Ready!");
            bodyCameraOn = true;
        }

        private void calc_distances_from_buffer_Click(object sender, RoutedEventArgs e)
        {
            if (frames != null)
            {
                DistanceCalculator calculator = new DistanceCalculator();
                calculator.Init();
                calculator.WorkWithFrames(frames);
                calculator.ShowResult();
            }
        }

        public void TurnBodyCameraOff()
        {
            bodyCameraOn = false;
        }

        public void TurnBodyCameraOn()
        {
            bodyCameraOn = true;
        }

        private void ToggleGestureRecognition(object sender, RoutedEventArgs e)
        {
            if (tracker.GetRecognitionBuffers().getDB().Count < MinimumDBSize)
            {
                MessageBox.Show("Database is too small for gesture recognition, gesture recognition is turned off!");
            }
            else
            {
                tracker.recognisingGestures = !tracker.recognisingGestures;
            }
        }

        private void MarkFrameButton_Click(object sender, RoutedEventArgs e)
        {
            MarkFrame();
        }

        private void SwitchMarker()
        {
            if (tracker.CurrentMarker == tracker.MarkerCount)
            {
                tracker.CurrentMarker = 0;
            }
            else
            {
                tracker.CurrentMarker = tracker.CurrentMarker + 1;
            }
            markerCount.Content = tracker.CurrentMarker + "(" + tracker.MarkerCount + ")";
        }

        private void extract_marked_gestures_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            bool? userClickedOK = openFileDialog1.ShowDialog();
            if (userClickedOK == true)
            {
                LogTools.LogMarkedGestures(File.ReadLines(openFileDialog1.FileName));
            }
        }

        private void Set_marker_count_click(object sender, RoutedEventArgs e)
        {
            this.Topmost = false;
            var input = Microsoft.VisualBasic.Interaction.InputBox("How many markers to use?", "Marker count", "1");
            int i;
            if (int.TryParse(input, out i))
            {
                tracker.MarkerCount = i;
            }
            this.Topmost = true;
            markerCount.Content = tracker.CurrentMarker + "(" + tracker.MarkerCount + ")";
        }

        private void Set_Recognised_Gesture(Buffers buffers, EventArgs e)
        {
            recognised_gesture.Content = buffers.calculator.Result();
        }
    }
}
